
package Controlador;

import javafx.beans.property.SimpleStringProperty;

public class Registro_Proveedores {
    
    private final SimpleStringProperty id;
    private final SimpleStringProperty descripcion; 
    private final SimpleStringProperty rif; 
    private final SimpleStringProperty telefono;
    private final SimpleStringProperty correo;
    
    public Registro_Proveedores(String idd, String descripcionn, String riff, String telf, String correoo){
        
        this.id=new SimpleStringProperty(idd);
        this.descripcion=new SimpleStringProperty(descripcionn);
        this.rif=new SimpleStringProperty(riff);
        this.telefono=new SimpleStringProperty(telf);
        this.correo=new SimpleStringProperty(correoo);
        
    }
    
    public String getId(){
        return id.get();
    }
    public String getDescripcion(){
        return descripcion.get();
    }
    public String getRif(){
        return rif.get();
    }
    public String getTelefono(){
        return telefono.get();
    }
    public String getCorreo(){
        return correo.get();
    }
    
    public void setId(String dato){
        id.set(dato);
    }
    public void setDescripcion(String dato){
        descripcion.set(dato);
    }
    public void setRif(String dato){
        rif.set(dato);
    }
    public void setTelefono(String dato){
        telefono.set(dato);
    }
    public void setEmail(String dato){
        correo.set(dato);
    }
    
}
