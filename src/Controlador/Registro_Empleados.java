package Controlador;

import javafx.beans.property.SimpleStringProperty;


public class Registro_Empleados {
    
    private final SimpleStringProperty id;
    private final SimpleStringProperty ficha;
    private final SimpleStringProperty nombre; 
    private final SimpleStringProperty cedula; 
    private final SimpleStringProperty telefono; 
    private final SimpleStringProperty direccion;
    private final SimpleStringProperty tipo;
    
    public Registro_Empleados(String idd, String fichaa, String nombree, String cedulaa, String telf, String direccionn, String tipoo){
        
        this.id=new SimpleStringProperty(idd);
        this.ficha=new SimpleStringProperty(fichaa);
        this.nombre=new SimpleStringProperty(nombree);
        this.cedula=new SimpleStringProperty(cedulaa);
        this.telefono=new SimpleStringProperty(telf);
        this.direccion=new SimpleStringProperty(direccionn);
        this.tipo=new SimpleStringProperty(tipoo);
        
    }
    
    public String getId(){
        return id.get();
    }
    public String getFicha(){
        return ficha.get();
    }
    public String getNombre(){
        return nombre.get();
    }
    public String getCedula(){
        return cedula.get();
    }
    public String getTelefono(){
        return telefono.get();
    }
    public String getDireccion(){
        return direccion.get();
    }
    public String getTipo(){
        return tipo.get();
    }
    
    public void setId(String dato){
        id.set(dato);
    }
    public void setFicha(String dato){
        ficha.set(dato);
    }
    public void setNombre(String dato){
        nombre.set(dato);
    }
    public void setCedula(String dato){
        cedula.set(dato);
    }
    public void setTelefono(String dato){
        telefono.set(dato);
    }
    public void setDireccion(String dato){
        direccion.set(dato);
    }
    public void setTipo(String dato){
        tipo.set(dato);
    }
    
}
