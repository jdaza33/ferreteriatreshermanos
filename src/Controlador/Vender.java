package Controlador;

import javafx.beans.property.SimpleStringProperty;


public class Vender {
    
    private final SimpleStringProperty id;
    private final SimpleStringProperty cod_interno; 
    private final SimpleStringProperty descripcion; 
    private final SimpleStringProperty precio; 
    private final SimpleStringProperty cantidad;
    private final SimpleStringProperty total;
    
    public Vender(String idd, String cod, String descripcionn, String precioo, String cant, String totall){
        
        this.id=new SimpleStringProperty(idd);
        this.cod_interno=new SimpleStringProperty(cod);
        this.descripcion=new SimpleStringProperty(descripcionn);
        this.precio=new SimpleStringProperty(precioo);
        this.cantidad=new SimpleStringProperty(cant);
        this.total=new SimpleStringProperty(totall);
        
    }
    
    public String getId(){
        return id.get();
    }
    public String getCod_interno(){
        return cod_interno.get();
    }
    public String getDescripcion(){
        return descripcion.get();
    }
    public String getPrecio(){
        return precio.get();
    }
    public String getCantidad(){
        return cantidad.get();
    }
    public String getTotal(){
        return total.get();
    }
    
    public void setId(String dato){
        id.set(dato);
    }
    public void setCod_interno(String dato){
        cod_interno.set(dato);
    }
    public void setDescripcion(String dato){
        descripcion.set(dato);
    }
    public void setPrecio(String dato){
        precio.set(dato);
    }
    public void setCantidad(String dato){
        cantidad.set(dato);
    }
    public void setTotal(String dato){
        total.set(dato);
    }
    
}
