package Modelo;

//CLASE PARA CONECTAR EL PROYECTO CON LA BASE DE DATOS

import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.digest.DigestUtils;


public class Conexion {
    
    static String url = "bd/ferreteria.db";
    static Connection connect;
    
    public static void conectar(){
     try {
     connect = DriverManager.getConnection("jdbc:sqlite:"+url);
     if (connect!=null) {
         System.out.println("Conectado");
     }
    }catch (SQLException ex) {
     System.err.println("No se ha podido conectar a la base de datos\n"+ex.getMessage());
        }
    }
    
    public static void cerrar(){
        try {
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean login(String cedula, String clave){
        
        boolean op=false;
        String aux_ced=DigestUtils.md5Hex(cedula); 
        String encClave = DigestUtils.md5Hex(clave);
        conectar();
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from login");
            result = st.executeQuery();
            while (result.next()) {
                
                String ced=result.getString("cedula");
               
                if(ced.compareTo(aux_ced) == 0){
                    String cla=result.getString("clave");
                    if((cla.compareTo(encClave))==0){
                        op=true;
                    }
                }else{
                    op=false;
                }
 
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        return op;
    }
    
    public ArrayList mostrarInventario(){
        
        ArrayList<String> aux=new ArrayList<String>();
        
        conectar();
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from articulos");
            result = st.executeQuery();
            while (result.next()) {
                
                int id=result.getInt("id");
                String descripcion=result.getString("descripcion");
                int grupo=result.getInt("id_grupo");
                int proveedor=result.getInt("id_proveedor");
                int cant=result.getInt("cantidad");
                double precio=result.getDouble("precio_base");
                String en=Integer.toString(id)+"|"+descripcion+"|"+Integer.toString(grupo)+"|"+Integer.toString(proveedor)+"|"+Integer.toString(cant)+"|"+Double.toString(precio);
                aux.add(en);
            
 
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        return aux;
    }
    
    public ArrayList mostrarClientes(){
        
        ArrayList<String> aux=new ArrayList<String>();
        
        conectar();
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from clientes");
            result = st.executeQuery();
            while (result.next()) {
                
                int id=result.getInt("id");
                String nombre=result.getString("nombre");
                int cedula=result.getInt("cedula");
                String telefono=result.getString("telefono_local");
                String celular=result.getString("telefono_celular");
                String correo=result.getString("correo");
                String en=Integer.toString(id)+"|"+nombre+"|"+Integer.toString(cedula)+"|"+telefono+"|"+celular+"|"+correo;
                aux.add(en);
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        return aux;
    }
    
    public ArrayList mostrarProveedoresRegistro(){
        
        ArrayList<String> aux=new ArrayList<String>();
        
        conectar();
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from proveedores");
            result = st.executeQuery();
            while (result.next()) {
                
                int id=result.getInt("id");
                String nombre=result.getString("descripcion");
                String cedula=result.getString("rif");
                String telefono=result.getString("telefono_uno");
                String correo=result.getString("correo");
                String en=Integer.toString(id)+"|"+nombre+"|"+cedula+"|"+telefono+"|"+correo;
                aux.add(en);
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        return aux;
    }
    
    public ArrayList mostrarEmpleadosRegistro(){
        
        ArrayList<String> aux=new ArrayList<String>();
        
        conectar();
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from empleados");
            result = st.executeQuery();
            while (result.next()) {
                
                int id=result.getInt("id");
                int ficha=result.getInt("ficha");
                String nombre=result.getString("nombres");
                String apellido=result.getString("apellidos");
                int cedula=result.getInt("cedula");
                String telefono=result.getString("telefono_local");
                String direccion=result.getString("direccion");
                String tipo=result.getString("tipo_empleado");
                String en=Integer.toString(id)+"|"+ Integer.toString(ficha) + "|" + nombre + " " + apellido+"|"+Integer.toString(cedula)+"|"+telefono+"|"+direccion+"|"+tipo;
                aux.add(en);
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        return aux;
    }
    
    public void agregarInventario(String datos[]){
        
        conectar();
        
        Statement stmt = null;
        Connection c = null;

        try{
        c = DriverManager.getConnection("jdbc:sqlite:"+url);
        c.setAutoCommit(false);
        stmt = c.createStatement();
        String sql = "INSERT INTO articulos (id, cod_interno, cod_externo, descripcion, marca, modelo, id_grupo, id_proveedor, cantidad, precio_base, fecha_entrada, ultima_modificacion, estatus, vendidos, devueltos, id_empleado)" 
                + "VALUES ("+Integer.parseInt(datos[0])+", '"+datos[1]+"', '"+datos[2]+"', '"+datos[3]+"', '"+datos[4]+"', '"+datos[5]+"', "+Integer.parseInt(datos[6])+", "+Integer.parseInt(datos[7])+", "+Integer.parseInt(datos[8])+", "+Double.parseDouble(datos[9])+", '"+datos[10]+"', '"+datos[11]+"', '"+datos[12]+"', "+Integer.parseInt(datos[13])+", "+Integer.parseInt(datos[14])+", "+Integer.parseInt(datos[15])+")";
        stmt.executeUpdate(sql);
        stmt.close();
        c.commit();
        c.close();
        }catch ( Exception e ){
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        
        cerrar();
    }
    
    public ArrayList mostrarGrupos(){
        
        ArrayList<String> aux=new ArrayList<String>();
        
        conectar();
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from articulos_grupo");
            result = st.executeQuery();
            while (result.next()) {
                
                int id=result.getInt("id");
                String nombre=result.getString("nombre");
                String en=Integer.toString(id)+"-"+nombre;
                aux.add(en);
            
 
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        return aux;
        
    }
    
    public ArrayList mostrarProveedores(){
        
        ArrayList<String> aux=new ArrayList<String>();
        
        conectar();
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from proveedores");
            result = st.executeQuery();
            while (result.next()) {
                
                int id=result.getInt("id");
                String descripcion=result.getString("descripcion");
                String en=Integer.toString(id)+"-"+descripcion;
                aux.add(en);
            
 
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        return aux;
        
    }
    
    public int generarId(String tabla){
        
        conectar();
        
        int aux=0;
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from "+tabla);
            result = st.executeQuery();
            while (result.next()) {
                
                aux=result.getInt("id");
            
 
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        aux=aux+1;
        
        return aux;
    }
    
    public ArrayList buscarCodInventario(int cod){
        
        conectar();
        
        ArrayList<String> datos=new ArrayList<String>();
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from articulos");
            result = st.executeQuery();
            while (result.next()) {
                
                int aux=result.getInt("id");
                if(aux==cod){
                    datos.add(Integer.toString(aux));
                    datos.add(result.getString("cod_interno"));
                    datos.add(result.getString("cod_externo"));
                    datos.add(result.getString("descripcion"));
                    datos.add(result.getString("marca"));
                    datos.add(result.getString("modelo"));
                    datos.add(Integer.toString(result.getInt("id_grupo")));
                    datos.add(Integer.toString(result.getInt("id_proveedor")));
                    datos.add(Integer.toString(result.getInt("cantidad")));
                    datos.add(Integer.toString((int) result.getDouble("precio_base")));
                    datos.add(result.getString("fecha_entrada"));
                    datos.add(result.getString("ultima_modificacion"));
                    datos.add(result.getString("estatus"));
                    datos.add(result.getString("vendidos"));
                    datos.add(result.getString("devueltos"));
                    datos.add(result.getString("id_empleado"));
                }
            
 
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        
        return datos;
        
    }
    
    public void modificarInventario(int id, ArrayList datos) throws SQLException{
        
        conectar();
        Statement stmt = null;
    
        try {
          stmt = connect.createStatement();
                String sql = "UPDATE articulos set cod_interno='"+datos.get(1)+"' , cod_externo='"+datos.get(2)+"' , descripcion='"+datos.get(3)+"' , marca='"+datos.get(4)+"' , modelo='"+datos.get(5)+"' , id_grupo="+Integer.parseInt((String) datos.get(6))+" , id_proveedor="+Integer.parseInt((String) datos.get(7))+" , cantidad="+Integer.parseInt((String) datos.get(8))+" , precio_base="+Double.parseDouble((String) datos.get(9))+" , fecha_entrada='"+datos.get(10)+"' , ultima_modificacion='"+datos.get(11)+"' , estatus='"+datos.get(12)+"' , vendidos="+Integer.parseInt((String) datos.get(13))+" , devueltos="+Integer.parseInt((String) datos.get(14))+" , id_empleado="+Integer.parseInt((String) datos.get(15))+" where id="+id;
                stmt.executeUpdate(sql);
                stmt.close();

        } catch ( Exception e ) {
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        
        cerrar();
        
    }
    
    public void eliminar(String id, String tabla){
        
        conectar();
        
        
        Statement stmt = null;
        try {
            connect.setAutoCommit(false);
            stmt = connect.createStatement();
            String sql = "DELETE from '"+tabla+"' where id="+id+";";
            stmt.executeUpdate(sql);
            connect.commit();
            stmt.close();
            connect.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        
        
        cerrar();
        
    }
    
    public ArrayList buscarArticulo(String cod_interno){
        
        conectar();
        
        ArrayList<String> datos=new ArrayList<String>();
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from articulos");
            result = st.executeQuery();
            while (result.next()) {
                
                int id=result.getInt("id");
                String cod_in = result.getString("cod_interno");
                int op=cod_in.compareTo(cod_interno);
                if(op==0){
                    
                    datos.add(Integer.toString(id));
                    datos.add(cod_in);
                    datos.add(result.getString("descripcion"));
                    datos.add(Integer.toString((int) result.getDouble("precio_base")));
                    datos.add(Integer.toString(result.getInt("cantidad")));
                   
                }
            
 
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        
        return datos;
        
    }
    
    public ArrayList buscarCliente(String cedula){
        
        conectar();
        
        ArrayList<String> datos=new ArrayList<String>();
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from clientes");
            result = st.executeQuery();
            while (result.next()) {
                
                int ced=result.getInt("cedula");
                int cedula_int = Integer.parseInt(cedula);
                if(ced==cedula_int){
                    datos.add(result.getString("nombre"));
                    datos.add(Integer.toString(cedula_int));
                }
 
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        
        return datos;
        
    }
    
    public void ingresarVentas(ArrayList<String> datos){
        
        conectar();
        
        Statement stmt = null;
        Connection c = null;

        try{
        c = DriverManager.getConnection("jdbc:sqlite:"+url);
        c.setAutoCommit(false);
        stmt = c.createStatement();
        String sql = "INSERT INTO ventas (id, nro, fecha, cliente, cedula, cant_articulos, subtotal, iva, total, vendido)" 
                + "VALUES ("+Integer.parseInt(datos.get(0))+", '"+datos.get(1)+"', '"+datos.get(2)+"', '"+datos.get(3)+"', '"+datos.get(4)+"', "+Integer.parseInt(datos.get(5))+", "+Double.parseDouble(datos.get(6))+", '"+datos.get(7)+"', "+Double.parseDouble(datos.get(8))+", '"+datos.get(9)+"')";
        stmt.executeUpdate(sql);
        stmt.close();
        c.commit();
        c.close();
        }catch ( Exception e ){
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        
        cerrar();
        
    }
    
    public void ingresarVentasArticulos(ArrayList<String> datos){
        
        conectar();
        
        Statement stmt = null;
        Connection c = null;

        try{
        c = DriverManager.getConnection("jdbc:sqlite:"+url);
        c.setAutoCommit(false);
        stmt = c.createStatement();
        String sql = "INSERT INTO ventas_articulos (id_venta, id, cod_interno, descripcion, precio, cant, total)" 
                + "VALUES ("+Integer.parseInt(datos.get(0))+", "+Integer.parseInt(datos.get(1))+", '"+datos.get(2)+"', '"+datos.get(3)+"', "+Double.parseDouble(datos.get(4))+", "+Integer.parseInt(datos.get(5))+", "+Double.parseDouble(datos.get(6))+")";
        stmt.executeUpdate(sql);
        stmt.close();
        c.commit();
        c.close();
        }catch ( Exception e ){
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        
        cerrar();
        
    }
    
    public double totalUltimaVenta(){
        double total=0;   
        conectar();
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from ventas");
            result = st.executeQuery();
            while (result.next()) {
                total=result.getDouble("total");
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        cerrar();
        return total;
    }
    
    public int idlUltimaVenta(){
        int id=0;   
        conectar();
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from ventas");
            result = st.executeQuery();
            while (result.next()) {
                id=result.getInt("id");
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        cerrar();
        return id;
    }
    
    public void ingresarVentasFormaPago(ArrayList<String> datos){
        
        conectar();
        
        Statement stmt = null;
        Connection c = null;

        try{
        c = DriverManager.getConnection("jdbc:sqlite:"+url);
        c.setAutoCommit(false);
        stmt = c.createStatement();
        String sql = "INSERT INTO ventas_forma_pago (id_venta, id, forma, monto, vuelto, comprobante)" 
                + "VALUES ("+Integer.parseInt(datos.get(0))+", "+Integer.parseInt(datos.get(1))+", '"+datos.get(2)+"', "+Double.parseDouble(datos.get(3))+", "+Double.parseDouble(datos.get(4))+", '"+datos.get(5)+"')";
        stmt.executeUpdate(sql);
        stmt.close();
        c.commit();
        c.close();
        }catch ( Exception e ){
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        
        cerrar();
        
    }
    
    public void estadoVentas(int id, String estado){
        
        conectar();
        Statement stmt = null;
    
        try {
          stmt = connect.createStatement();
                String sql = "UPDATE ventas set vendido='"+estado+"' where id="+id;
                stmt.executeUpdate(sql);
                stmt.close();

        } catch ( Exception e ) {
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        
        cerrar();
        
    }
    
    public void buscarArticuloRestar(int id){
        
        conectar();
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from ventas_articulos");
            result = st.executeQuery();
            while (result.next()) {
                int id_venta=result.getInt("id_venta");
                if(id==id_venta){
                    String cod_interno=result.getString("cod_interno");
                    int cantidad=result.getInt("cant");
                    restarInventario(cod_interno,cantidad);
                }
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        cerrar();
        
    }
    
    
    public void restarInventario(String cod_interno, int cant){
        
        int cant_actual=0;
        
        conectar();
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from articulos");
            result = st.executeQuery();
            while (result.next()) {
                String cod=result.getString("cod_interno");
                int op=cod.compareTo(cod_interno);
                if(op==0){
                    cant_actual=result.getInt("cantidad");
                }
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        cerrar();
        
        conectar();
        Statement stmt = null;
    
        try {
          stmt = connect.createStatement();
            int cant_futuro=cant_actual-cant;
            String cf=Integer.toString(cant_futuro);
            System.out.println(cf);
            System.out.println(cod_interno);
            String sql = "UPDATE articulos set cantidad='"+cf+"' where cod_interno='"+cod_interno+"'";
            stmt.executeUpdate(sql);
            stmt.close();

        } catch ( Exception e ) {
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        
        cerrar();
        
    }
    
    public void ingresarRrhh(ArrayList<String> datos){
        
        conectar();
        
        Statement stmt = null;
        Connection c = null;

        try{
        c = DriverManager.getConnection("jdbc:sqlite:"+url);
        c.setAutoCommit(false);
        stmt = c.createStatement();
        String sql = "INSERT INTO rrhh (id, cedula, sueldo_base, cesta_ticket, fecha_pago, contrato, fecha_ingreso,  cargo, ivss, lph, paro_forzoso)" 
                + "VALUES ("+Integer.parseInt(datos.get(0))+", "+Integer.parseInt(datos.get(1))+", "+Double.parseDouble(datos.get(2))+", "+Double.parseDouble(datos.get(3))+", '"+datos.get(4)+"', '"+datos.get(5)+"' , '"+datos.get(6)+"', '"+datos.get(7)+"', "+Double.parseDouble(datos.get(8))+", "+Double.parseDouble(datos.get(9))+", "+Double.parseDouble(datos.get(10))+")";
        stmt.executeUpdate(sql);
        stmt.close();
        c.commit();
        c.close();
        }catch ( Exception e ){
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        
        cerrar();
        
    }
    
    
    public ArrayList buscarEmpleado(String cedula){
        
        conectar();
        
        ArrayList<String> datos=new ArrayList<String>();
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from empleados");
            result = st.executeQuery();
            while (result.next()) {
                
                int ced=result.getInt("cedula");
                int cedula_int = Integer.parseInt(cedula);
                if(ced==cedula_int){
                    datos.add(result.getString("nombres"));
                    datos.add(Integer.toString(cedula_int));
                }
 
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        
        return datos;
    }
    
    public int getCantNomina(){
        
        conectar();
        
        int nro=0;
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from rrhh");
            result = st.executeQuery();
            while (result.next()) {
                nro=result.getInt("id");
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        
        return nro;
        
    }
    
    public String getNombre(String cedula){
        
        conectar();
        
        String nombre=null;
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from empleados");
            result = st.executeQuery();
            while (result.next()) {
                
                int ced=result.getInt("cedula");
                int cedula_int = Integer.parseInt(cedula);
                if(ced==cedula_int){
                    nombre=result.getString("nombres");
                }
 
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        
        return nombre;
        
    }
    
    public ArrayList getNomina(int id){

        conectar();
        
        ArrayList<String> datos=new ArrayList<String>();
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from rrhh");
            result = st.executeQuery();
            while (result.next()) {
                
                int id_aux=result.getInt("id");
                if(id==id_aux){
                    datos.add(Integer.toString(result.getInt("cedula")));
                    datos.add(result.getString("cargo"));
                    datos.add(Double.toString(result.getDouble("sueldo_base")));
                    datos.add(Double.toString(result.getDouble("ivss")));
                    datos.add(Double.toString(result.getDouble("lph")));
                    datos.add(Double.toString(result.getDouble("paro_forzoso")));
                }
                
                
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        
        return datos;
        
    }
    
    public ArrayList datosEmpresa(){

        conectar();
        
        ArrayList<String> datos=new ArrayList<String>();
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from empresa");
            result = st.executeQuery();
            while (result.next()) {
                    datos.add(result.getString("nombre"));
                    datos.add(result.getString("rif"));
                    datos.add(result.getString("direccion"));
                    datos.add(result.getString("telefono")); 
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        
        return datos;
        
    }
    
    public ArrayList datosVenta(int id){

        conectar();
        
        ArrayList<String> datos=new ArrayList<String>();
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from ventas");
            result = st.executeQuery();
            while (result.next()) {
                int aux_id=result.getInt("id");
                if(id==aux_id){
                datos.add(result.getString("nro"));
                datos.add(result.getString("fecha"));
                datos.add(result.getString("cliente"));
                datos.add(result.getString("cedula")); 
                datos.add(Double.toString(result.getDouble("subtotal"))); 
                datos.add(Double.toString(result.getDouble("total"))); 
                }
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        
        return datos;
        
    }
    
    public ArrayList datosVentaArticulos(int id){

        conectar();
        
        ArrayList<String> datos=new ArrayList<String>();
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from ventas_articulos");
            result = st.executeQuery();
            while (result.next()) {
                int aux_id=result.getInt("id_venta");
                if(id==aux_id){
                String nombre =result.getString("descripcion");
                int cant = result.getInt("cant");
                int total = result.getInt("total");
                String dato=nombre+"    "+Integer.toString(cant)+"  "+"Bs. "+Integer.toString(total);
                datos.add(dato);
                }
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        
        return datos;
        
    }
    
    public ArrayList datosVentaForma(int id){

        conectar();
        
        ArrayList<String> datos=new ArrayList<String>();
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from ventas_forma_pago");
            result = st.executeQuery();
            while (result.next()) {
                int aux_id=result.getInt("id_venta");
                if(id==aux_id){
                String forma =result.getString("forma");
                int monto = result.getInt("monto");
                String dato=forma+"    "+"Bs. "+Integer.toString(monto);
                datos.add(dato);
                }
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        
        return datos;
        
    }
    
    public ArrayList ingresosMensual(int mes){

        conectar();
        
        ArrayList<String> datos=new ArrayList<String>();
        double acum=0;
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from ventas");
            result = st.executeQuery();
            while (result.next()) {
                String fecha=result.getString("fecha");
                int op=0;
                StringTokenizer separar = new StringTokenizer(fecha,"-"); 
                int i=1;
                while (separar.hasMoreTokens()) {  
                    String aux_mes=separar.nextToken();
                    if(i==2){
                        op=Integer.parseInt(aux_mes);
                    } 
                    i++;
                }  
                if(op==mes){
                    String dato=result.getString("fecha")+"---------"+result.getDouble("total");
                    acum=acum+result.getDouble("total");
                    datos.add(dato);
                }
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        datos.add(Double.toString(acum));
        return datos;
        
    }
    
    public ArrayList reporteZ(String fecha_actual){

        conectar();
        
        ArrayList<String> datos=new ArrayList<String>();
        double acum=0;
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from ventas");
            result = st.executeQuery();
            while (result.next()) {
                String fecha=result.getString("fecha");
                int op=fecha.compareTo(fecha_actual);
                if(op==0){
                    String dato=result.getString("nro")+"---------"+result.getString("fecha")+"---------"+result.getDouble("total");
                    acum=acum+result.getDouble("total");
                    datos.add(dato);
                }
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        cerrar();
        
        datos.add(Double.toString(acum));
        return datos;
        
    }
    
    public void ingresarCliente(ArrayList<String> datos){
        
        conectar();
        
        Statement stmt = null;
        Connection c = null;

        try{
        c = DriverManager.getConnection("jdbc:sqlite:"+url);
        c.setAutoCommit(false);
        stmt = c.createStatement();
        String sql = "INSERT INTO clientes (id, nombre, cedula, direccion, telefono_local, telefono_celular, correo,  fecha_registro, ultima_modificacion, id_empleado, foto)" 
                + "VALUES ("+Integer.parseInt(datos.get(0))+", '"+datos.get(1)+"', "+Integer.parseInt(datos.get(2))+", '"+datos.get(3)+"', '"+datos.get(4)+"', '"+datos.get(5)+"' , '"+datos.get(6)+"', '"+datos.get(7)+"', '"+datos.get(8)+"', "+Integer.parseInt(datos.get(9))+", '"+datos.get(10)+"')";
        stmt.executeUpdate(sql);
        stmt.close();
        c.commit();
        c.close();
        }catch ( Exception e ){
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        
        cerrar();
        
    }
    
    public void ingresarProveedores(ArrayList<String> datos){
        
        conectar();
        
        Statement stmt = null;
        Connection c = null;

        try{
        c = DriverManager.getConnection("jdbc:sqlite:"+url);
        c.setAutoCommit(false);
        stmt = c.createStatement();
        String sql = "INSERT INTO proveedores (id, descripcion, rif, telefono_uno, telefono_dos, correo, fecha_registro, ultima_entrega, id_empleado, logo)" 
                + "VALUES ("+Integer.parseInt(datos.get(0))+", '"+datos.get(1)+"', '"+datos.get(2)+"', '"+datos.get(3)+"', '"+datos.get(4)+"' , '"+datos.get(5)+"', '"+datos.get(6)+"', '"+datos.get(7)+"', "+Integer.parseInt(datos.get(8))+", '"+datos.get(9)+"')";
        stmt.executeUpdate(sql);
        stmt.close();
        c.commit();
        c.close();
        }catch ( Exception e ){
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        
        cerrar();
        
    }
    
    public void ingresarEmpleados(ArrayList<String> datos){
        
        conectar();
        
        Statement stmt = null;
        Connection c = null;

        try{
        c = DriverManager.getConnection("jdbc:sqlite:"+url);
        c.setAutoCommit(false);
        stmt = c.createStatement();
        String sql = "INSERT INTO empleados (id, ficha, nombres, apellidos, cedula, rif, direccion, fecha_nacimiento, telefono_local, telefono_celular, correo, tipo_empleado, id_rrhh, fecha_registro, ultima_modificacion, foto)" 
                + "VALUES ("+Integer.parseInt(datos.get(0))+", "+Integer.parseInt(datos.get(1))+", '"+datos.get(2)+"', '"+datos.get(3)+"', "+Integer.parseInt(datos.get(4))+" , '"+datos.get(5)+"', '"+datos.get(6)+"', '"+datos.get(7)+"', '"+datos.get(8)+"', '"+datos.get(9)+"', '"+datos.get(10)+"', '"+datos.get(11)+"', '"+datos.get(12)+"', '"+datos.get(13)+"', '"+datos.get(14)+"', '"+datos.get(15)+"')";
        stmt.executeUpdate(sql);
        stmt.close();
        c.commit();
        c.close();
        }catch ( Exception e ){
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        
        cerrar();
        
    }
    
}
