package Vista;

import Modelo.Conexion;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javax.swing.JOptionPane;

public class FacturacionController implements Initializable {

    @FXML
    private AnchorPane ventana_facturacion;
    @FXML
    private CheckBox forma_efectivo;
    @FXML
    private CheckBox forma_punto;
    @FXML
    private JFXTextField monto_efectivo;
    @FXML
    private JFXTextField total;
    @FXML
    private JFXTextField vuelto;
    @FXML
    private JFXTextField monto_punto;
    @FXML
    private JFXTextField comprobante;
    @FXML
    private JFXTextField ultimos_digitos;
    @FXML
    private JFXTextField titular;
    @FXML
    private JFXButton btn_facturar;
    @FXML
    private ProgressBar progreso;
    @FXML
    private CheckBox forma_ambas;
    @FXML
    private JFXButton btn_volver;
    @FXML
    private VBox vbox_efectivo;
    @FXML
    private VBox vbox_punto;

    Funciones f=new Funciones();
    Conexion c=new Conexion();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        validarForma();
        
        
        monto_efectivo.setStyle("-fx-prompt-text-fill: #424242;"); 
        total.setStyle("-fx-prompt-text-fill: #424242;"); 
        vuelto.setStyle("-fx-prompt-text-fill: #424242;"); 
        monto_punto.setStyle("-fx-prompt-text-fill: #424242;"); 
        comprobante.setStyle("-fx-prompt-text-fill: #424242;"); 
        ultimos_digitos.setStyle("-fx-prompt-text-fill: #424242;"); 
        titular.setStyle("-fx-prompt-text-fill: #424242;"); 
        
        //Mostrar total
        total.setText(Double.toString(c.totalUltimaVenta()));
        monto_punto.setText(Double.toString(c.totalUltimaVenta()));
        total.setDisable(true);
        monto_punto.setDisable(true);
        
    }    
    
    @FXML
    public void volver(){
        
        f.cerrarSolamente(ventana_facturacion);
        
    }
    
    @FXML
    public void validarForma(){
        
        
        boolean fe=forma_efectivo.isSelected();
        boolean fp=forma_punto.isSelected();
        boolean fa=forma_ambas.isSelected();
        
        if(fe==true && fa==false && fp==false){
            forma_punto.setDisable(true);
            forma_ambas.setDisable(true);
            vbox_efectivo.setDisable(false);
            btn_facturar.setDisable(false);
            progreso.setDisable(false);
        }
        if(fe==false && fp==false && fe==false){
            forma_punto.setDisable(false);
            forma_ambas.setDisable(false);
            vbox_efectivo.setDisable(true);
            btn_facturar.setDisable(true);
            progreso.setDisable(true);
        }
        if(fp==true && fa==false && fe==false){
            forma_efectivo.setDisable(true);
            forma_ambas.setDisable(true);
            vbox_punto.setDisable(false);
            btn_facturar.setDisable(false);
            progreso.setDisable(false);
        }
        if(fp==false && fp==false && fe==false){
            forma_efectivo.setDisable(false);
            forma_ambas.setDisable(false);
            vbox_punto.setDisable(true);
            btn_facturar.setDisable(true);
            progreso.setDisable(true);
        }
        if(fa==true && fp==false && fe==false){
            forma_punto.setDisable(true);
            vbox_efectivo.setDisable(false);
            btn_facturar.setDisable(false);
            progreso.setDisable(false);
            forma_efectivo.setDisable(true);
            vbox_punto.setDisable(false);
        }
        

    }
    
    @FXML
    public void calcularVuelto(){
        
        String aux_me=monto_efectivo.getText();
        int tam=aux_me.length();
        if(tam!=0){
            double to=Double.parseDouble(total.getText());
            int res=(int) (Double.parseDouble(aux_me)-to);
            vuelto.setText(Double.toString(res));
        }
        
        
    }
    
    @FXML
    public void facturar() throws IOException{
        
        
        
        boolean fe=forma_efectivo.isSelected();
        boolean fp=forma_punto.isSelected();
        boolean fa=forma_ambas.isSelected();
        
        if(fe==true){
            ArrayList<String> aux=new ArrayList<String>();
            aux.add(Integer.toString(c.idlUltimaVenta()));
            aux.add("1");
            aux.add("EFECTIVO");
            aux.add(Double.toString(c.totalUltimaVenta()));
            aux.add(vuelto.getText());
            aux.add("0");
            c.ingresarVentasFormaPago(aux);
            
            JOptionPane.showMessageDialog(null, "VENTA EXITOSA.");
        }
        
        if(fp==true){
            ArrayList<String> aux=new ArrayList<String>();
            aux.add(Integer.toString(c.idlUltimaVenta()));
            aux.add("1");
            aux.add("PUNTO DE VENTA");
            aux.add(Double.toString(c.totalUltimaVenta()));
            aux.add("0");
            aux.add(comprobante.getText());
            c.ingresarVentasFormaPago(aux);
            
            JOptionPane.showMessageDialog(null, "VENTA EXITOSA.");
        }
        
        if(fa==true){
            ArrayList<String> aux=new ArrayList<String>();
            ArrayList<String> aux2=new ArrayList<String>();
            
            aux.add(Integer.toString(c.idlUltimaVenta()));
            aux.add("1");
            aux.add("EFECTIVO");
            aux.add(Double.toString(c.totalUltimaVenta()));
            aux.add(vuelto.getText());
            aux.add("0");
            
            aux2.add(Integer.toString(c.idlUltimaVenta()));
            aux2.add("2");
            aux2.add("PUNTO DE VENTA");
            aux2.add(Double.toString(c.totalUltimaVenta()));
            aux2.add("0");
            aux2.add(comprobante.getText());
            
            c.ingresarVentasFormaPago(aux);
            c.ingresarVentasFormaPago(aux2);
            
            JOptionPane.showMessageDialog(null, "VENTA EXITOSA.");
        }
        //c.buscarArticuloRestar(c.idlUltimaVenta());
        c.estadoVentas(c.idlUltimaVenta(), "S");
        generarFactura((int) c.idlUltimaVenta());
        Funciones funciones=new Funciones(1);
        volver();
    }
    
    @FXML
    public void generarFactura(int id) throws IOException{
        
        ArrayList<String> empresa=c.datosEmpresa();
        ArrayList<String> venta=c.datosVenta(id);
        ArrayList<String> venta_articulos=c.datosVentaArticulos(id);
        ArrayList<String> venta_pago=c.datosVentaForma(id);
        int tam=venta.size();
        if(tam>3){
            FileWriter fichero = null;
            fichero = new FileWriter("Datos/ventas/"+venta.get(0)+".txt");
            f.factura(fichero, empresa, venta, venta_articulos, venta_pago);
            JOptionPane.showMessageDialog(null, "FACTURA GENERADA CON EXITO.");
        }
    }
    
}
