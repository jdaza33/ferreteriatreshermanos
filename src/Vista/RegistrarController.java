package Vista;

import Controlador.Inventario;
import Controlador.Registro_Clientes;
import Controlador.Registro_Empleados;
import Controlador.Registro_Proveedores;
import Modelo.Conexion;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;

public class RegistrarController implements Initializable {

    @FXML
    private AnchorPane ventana_registrar;
    @FXML
    private JFXHamburger menu;
    @FXML
    private JFXButton btn_agregar;
    @FXML
    private JFXButton btn_modificar;
    @FXML
    private JFXButton btn_eliminar;
    @FXML
    private JFXButton btn_buscar;
    @FXML
    private JFXButton btn_salir;
    @FXML
    private JFXDrawer draww;
    @FXML
    private JFXComboBox<String> item;
    
    @FXML
    private TableView<Registro_Clientes> tabla_clientes=new TableView<Registro_Clientes>();
    @FXML
    private TableView<Registro_Proveedores> tabla_proveedores=new TableView<Registro_Proveedores>();
    @FXML
    private TableView<Registro_Empleados> tabla_empleados=new TableView<Registro_Empleados>();
    

    @FXML
    private TableColumn<Registro_Clientes, String> celular_clientes;
    @FXML
    private TableColumn<Registro_Clientes, String> correo_clientes;
    @FXML
    private TableColumn<Registro_Clientes, String> id_clientes;
    @FXML
    private TableColumn<Registro_Clientes, String> telefono_clientes;
    @FXML
    private TableColumn<Registro_Clientes, String> cedula_clientes;
    @FXML
    private TableColumn<Registro_Clientes, String> nombre_clientes;
    
    @FXML
    private TableColumn<Registro_Empleados, String> ficha_empleados;
    @FXML
    private TableColumn<Registro_Empleados, String> cedula_empleados;
    @FXML
    private TableColumn<Registro_Empleados, String> direccion_empleados;
    @FXML
    private TableColumn<Registro_Empleados, String> id_empleados;
    @FXML
    private TableColumn<Registro_Empleados, String> nombre_empleados;
    @FXML
    private TableColumn<Registro_Empleados, String> telefono_empleados;
    @FXML
    private TableColumn<Registro_Empleados, String> tipo_empleados;
        
    @FXML
    private TableColumn<Registro_Proveedores, String> rif_proveedor;
    @FXML
    private TableColumn<Registro_Proveedores, String> telefono_proveedor;
    @FXML
    private TableColumn<Registro_Proveedores, String> id_proveedor;
    @FXML
    private TableColumn<Registro_Proveedores, String> descripcion_proveedor;
    @FXML
    private TableColumn<Registro_Proveedores, String> correo_proveedor;
    
    final ObservableList<Registro_Clientes> data_clientes = FXCollections.observableArrayList(); 
    final ObservableList<Registro_Proveedores> data_proveedores = FXCollections.observableArrayList(); 
    final ObservableList<Registro_Empleados> data_empleados = FXCollections.observableArrayList(); 

    Funciones f= new Funciones();
    Conexion c= new Conexion();
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        draww.setVisible(false);
        
         //------CODIGO PARA ABRIR EL MENU
        try {
            AnchorPane box = FXMLLoader.load(getClass().getResource("subpanel.fxml"));
            draww.setSidePane(box);
        } catch (IOException ex) {
            Logger.getLogger(PanelController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        HamburgerBackArrowBasicTransition transition = new HamburgerBackArrowBasicTransition(menu);
        transition.setRate(-1);
        menu.addEventHandler(MouseEvent.MOUSE_PRESSED,(e)->{
            draww.setVisible(true);
            transition.setRate(transition.getRate()*-1);
            transition.play();
            
            if(draww.isShown())
            {
                draww.close();
                draww.setVisible(false);
            }else{
                draww.open();
            }
        });
        //FIN DEL CODIGO PARA ABRIR EL MENU
        
        //Para insertar imagen en los botones
        
        Image image2 = new Image(getClass().getResourceAsStream("icon\\agregar.png"));
        btn_agregar.setGraphic(new ImageView(image2));
        
        Image image3 = new Image(getClass().getResourceAsStream("icon\\modificar.png"));
        btn_modificar.setGraphic(new ImageView(image3));
        
        Image image4 = new Image(getClass().getResourceAsStream("icon\\eliminar.png"));
        btn_eliminar.setGraphic(new ImageView(image4));
        
        Image image5 = new Image(getClass().getResourceAsStream("icon\\salir.png"));
        btn_salir.setGraphic(new ImageView(image5));
        
        //Ingresar Items
        ingresarItem();
        
        
    }    
    
    @FXML
    public void ingresarItem(){
        
        item.getItems().add("Clientes");
        item.getItems().add("Proveedores");
        item.getItems().add("Empleados");
       
        
    }
    
    
    @FXML
    public void ingresarDatosClientes(){
        
        limpiarTablas();
        
        tabla_proveedores.setVisible(false);
        tabla_empleados.setVisible(false);
        tabla_clientes.setVisible(true);
        
        id_clientes.setCellValueFactory(new PropertyValueFactory<Registro_Clientes, String>("id"));
        nombre_clientes.setCellValueFactory(new PropertyValueFactory<Registro_Clientes, String>("nombre"));
        cedula_clientes.setCellValueFactory(new PropertyValueFactory<Registro_Clientes, String>("cedula"));
        telefono_clientes.setCellValueFactory(new PropertyValueFactory<Registro_Clientes, String>("telefono"));
        celular_clientes.setCellValueFactory(new PropertyValueFactory<Registro_Clientes, String>("celular"));
        correo_clientes.setCellValueFactory(new PropertyValueFactory<Registro_Clientes, String>("correo"));
        
        tabla_clientes.setItems(data_clientes);
        
        ArrayList<String> aux=c.mostrarClientes();
        
        for (int i=0; i<aux.size(); i++){
            
            StringTokenizer st = new StringTokenizer(aux.get(i),"|");
            ArrayList<String> temp=new ArrayList<String>();
            
            while (st.hasMoreTokens()) {  
                temp.add(st.nextToken());
            }  
            
            data_clientes.add(new Registro_Clientes(temp.get(0),temp.get(1),temp.get(2),temp.get(3),temp.get(4),temp.get(5)));

        }
        
    }
    
    @FXML
    public void ingresarDatosProveedores(){
        
        limpiarTablas();
        
        tabla_empleados.setVisible(false);
        tabla_clientes.setVisible(false);
        tabla_proveedores.setVisible(true);
        
        id_proveedor.setCellValueFactory(new PropertyValueFactory<Registro_Proveedores, String>("id"));
        descripcion_proveedor.setCellValueFactory(new PropertyValueFactory<Registro_Proveedores, String>("descripcion"));
        rif_proveedor.setCellValueFactory(new PropertyValueFactory<Registro_Proveedores, String>("rif"));
        telefono_proveedor.setCellValueFactory(new PropertyValueFactory<Registro_Proveedores, String>("telefono"));
        correo_proveedor.setCellValueFactory(new PropertyValueFactory<Registro_Proveedores, String>("correo"));
        
        tabla_proveedores.setItems(data_proveedores);
        
        ArrayList<String> aux=c.mostrarProveedoresRegistro();
        
        for (int i=0; i<aux.size(); i++){
            
            StringTokenizer st = new StringTokenizer(aux.get(i),"|");
            ArrayList<String> temp=new ArrayList<String>();
            
            while (st.hasMoreTokens()) {  
                temp.add(st.nextToken());
            }  
            
            data_proveedores.add(new Registro_Proveedores(temp.get(0),temp.get(1),temp.get(2),temp.get(3),temp.get(4)));

        }
        
    }
    
    @FXML
    public void ingresarDatosEmpleados(){
        
        limpiarTablas();
        
        tabla_clientes.setVisible(false);
        tabla_proveedores.setVisible(false);
        tabla_empleados.setVisible(true);
        
        
        id_empleados.setCellValueFactory(new PropertyValueFactory<Registro_Empleados, String>("id"));
        ficha_empleados.setCellValueFactory(new PropertyValueFactory<Registro_Empleados, String>("ficha"));
        nombre_empleados.setCellValueFactory(new PropertyValueFactory<Registro_Empleados, String>("nombre"));
        cedula_empleados.setCellValueFactory(new PropertyValueFactory<Registro_Empleados, String>("cedula"));
        telefono_empleados.setCellValueFactory(new PropertyValueFactory<Registro_Empleados, String>("telefono"));
        direccion_empleados.setCellValueFactory(new PropertyValueFactory<Registro_Empleados, String>("direccion"));
        tipo_empleados.setCellValueFactory(new PropertyValueFactory<Registro_Empleados, String>("tipo"));
        
        tabla_empleados.setItems(data_empleados);
        
        ArrayList<String> aux=c.mostrarEmpleadosRegistro();
        
        for (int i=0; i<aux.size(); i++){
            
            StringTokenizer st = new StringTokenizer(aux.get(i),"|");
            ArrayList<String> temp=new ArrayList<String>();
            
            while (st.hasMoreTokens()) {  
                temp.add(st.nextToken());
            }  
            
            data_empleados.add(new Registro_Empleados(temp.get(0),temp.get(1),temp.get(2),temp.get(3),temp.get(4),temp.get(5),temp.get(6)));

        }
        
    }
    
    @FXML
    public void limpiarTablas(){
        
        //Borrar filas
            for ( int i = 0; i<tabla_clientes.getItems().size(); i++) {
            tabla_clientes.getItems().clear();
            }
            
            for ( int i = 0; i<tabla_proveedores.getItems().size(); i++) {
            tabla_proveedores.getItems().clear();
            }
            
            for ( int i = 0; i<tabla_empleados.getItems().size(); i++) {
            tabla_empleados.getItems().clear();
            }
        
    }
    
    @FXML
    public void actualizarTipoTabla(){
        
        String a, b, c;
        a="Clientes";
        b="Proveedores";
        c="Empleados";
        String aux=(String) item.getValue();

        int op=aux.compareTo(a);
        int op2=aux.compareTo(b);
        int op3=aux.compareTo(c);
        
        if(op==0){
            ingresarDatosClientes();
        }
        if(op2==0){
            ingresarDatosProveedores();
        }
        if(op3==0){
            ingresarDatosEmpleados();
        }
        
    }
    
    @FXML
    public void agregarRegistro() throws IOException{
        
        f.abrirSolamente("registrar_agregar.fxml");
        
    }
    
    @FXML
    public void salir(){
        
        int ax = JOptionPane.showConfirmDialog(null, "¿Desea salir?");
        if(ax == JOptionPane.YES_OPTION){
            System.exit(0);
            /*timerTask.cancel();
            timer.cancel();*/
        }

    }
    
    @FXML
    public ArrayList extraer(){
        
        ArrayList<String> dato=new ArrayList<String>();
        String a, b, c;
        a="Clientes";
        b="Proveedores";
        c="Empleados";
        String aux=(String) item.getValue();

        int op=aux.compareTo(a);
        int op2=aux.compareTo(b);
        int op3=aux.compareTo(c);
        
        if(op==0){
            try {
            TablePosition pos = tabla_clientes.getSelectionModel().getSelectedCells().get(0);
            int row = pos.getRow();
            dato.add((String) id_clientes.getCellObservableValue(row).getValue());
            dato.add("clientes");
            } catch ( Exception e ) {
            System.out.println("ERROR AL SELECCIONAR ITEM");
            }
        }
        if(op2==0){
            try {
            TablePosition pos = tabla_proveedores.getSelectionModel().getSelectedCells().get(0);
            int row = pos.getRow();
            dato.add((String) id_proveedor.getCellObservableValue(row).getValue());
            dato.add("proveedores");
            } catch ( Exception e ) {
            System.out.println("ERROR AL SELECCIONAR ITEM");
            }
        }
        if(op3==0){
            try {
            TablePosition pos = tabla_empleados.getSelectionModel().getSelectedCells().get(0);
            int row = pos.getRow();
            dato.add((String) id_empleados.getCellObservableValue(row).getValue());
            dato.add("empleados");
            } catch ( Exception e ) {
            System.out.println("ERROR AL SELECCIONAR ITEM");
            }
            
        }
        return dato;
    }
    
    @FXML
    public void eliminar(){
        
        ArrayList<String> dato=extraer();
        int tam=dato.size();
        if(tam>1){
            int ax = JOptionPane.showConfirmDialog(null, "¿Desea Eliminar este Dato?");
            if(ax == JOptionPane.YES_OPTION){
                c.eliminar(dato.get(0), dato.get(1));
                //Funciones funciones=new Funciones(1);
                JOptionPane.showMessageDialog(null, "Dato Eliminado, por favor actualice.");
            }
        }else{
            JOptionPane.showMessageDialog(null, "Por favor seleccione una fila...");
        }
        
        
    }
    
}
