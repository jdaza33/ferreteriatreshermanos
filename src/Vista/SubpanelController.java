
package Vista;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class SubpanelController implements Initializable  {

    @FXML
    private ImageView foto;
    @FXML
    private Label nombre;
    @FXML
    private JFXButton btn1;
    @FXML
    private JFXButton btn2;
    @FXML
    private JFXButton btn3;
    @FXML
    private JFXButton btn4;
    @FXML
    private JFXButton btn5;
    @FXML
    private JFXButton btn6;
    @FXML
    private AnchorPane ventana_subpanel;

    Funciones f=new Funciones();
    PanelController pc=new PanelController();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Image image = new Image(getClass().getResourceAsStream("icon\\casa.png"));
        Image image2 = new Image(getClass().getResourceAsStream("icon\\almacen.png"));
        Image image3 = new Image(getClass().getResourceAsStream("icon\\ventas.png"));
        Image image4 = new Image(getClass().getResourceAsStream("icon\\registrar.png"));
        Image image5 = new Image(getClass().getResourceAsStream("icon\\grupo.png"));
        Image image6 = new Image(getClass().getResourceAsStream("icon\\mas.png"));
        //btn1.setStyle("-fx-background-color: #83C8FF;"); 
        //btn1.setStyle("-fx-background-color: rgba(131, 200, 255, 1);");
        /*btn2.setStyle("-fx-background-color: #77D4E8;"); 
        btn3.setStyle("-fx-background-color: #90FFF5;");
        btn4.setStyle("-fx-background-color: #77E8BD;"); 
        btn5.setStyle("-fx-background-color: #83FFAD;"); */
        btn1.setGraphic(new ImageView(image));
        btn2.setGraphic(new ImageView(image2));
        btn3.setGraphic(new ImageView(image3));
        btn4.setGraphic(new ImageView(image4));
        btn5.setGraphic(new ImageView(image5));
        btn6.setGraphic(new ImageView(image6));
        
        //-------------------------------------
        
       
    }    
    
    @FXML
    public void inventario() throws IOException{
        f.intercambio("inventario.fxml",ventana_subpanel);
    }
    @FXML
    public void ventas() throws IOException{
        f.intercambio("ventas.fxml",ventana_subpanel);
    }
    @FXML
    public void home() throws IOException{
        f.intercambio("panel.fxml",ventana_subpanel);
    }
    @FXML
    public void registrar() throws IOException{
        f.intercambio("registrar.fxml",ventana_subpanel);
    }
    @FXML
    public void rrhh() throws IOException{
        f.intercambio("rrhh.fxml",ventana_subpanel);
    }
    @FXML
    public void mas() throws IOException{
        f.abrirSolamente("mas.fxml");
    }
    
}
