
package Vista;

import Modelo.Conexion;
import com.jfoenix.controls.JFXButton;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;


public class MasController implements Initializable {

    @FXML
    private AnchorPane ventana_agregar_inventario;
    @FXML
    private JFXButton btn_salir;
    @FXML
    private JFXButton btn_totalMes;
    @FXML
    private JFXButton Z;
    @FXML
    private JFXButton nomina;
    
    Date date = new Date();
    int año=date.getYear()+1900;
    int mes=date.getMonth()+1;
    int dia=date.getDate();
    String fecha_actual=año+"-"+mes+"-"+dia;
    
    Funciones f=new Funciones();
    Conexion c=new Conexion();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        Image image = new Image(getClass().getResourceAsStream("icon\\nomina.png"));
        nomina.setGraphic(new ImageView(image));
        Image image5 = new Image(getClass().getResourceAsStream("icon\\salir.png"));
        btn_salir.setGraphic(new ImageView(image5));
        Image image2 = new Image(getClass().getResourceAsStream("icon\\reportar.png"));
        btn_totalMes.setGraphic(new ImageView(image2));
        Image image3 = new Image(getClass().getResourceAsStream("icon\\reporteZ.png"));
        Z.setGraphic(new ImageView(image3));
        
    }    

    @FXML
    public void salir(){
        
        f.cerrarSolamente(ventana_agregar_inventario);
        
    }
    
    @FXML
    public void generarNomina() throws IOException{
        
        String fecha = JOptionPane.showInputDialog("Ingrese el mes");
        FileWriter fichero = null;
        fichero = new FileWriter("Datos/nominas/"+fecha+".txt");
        JOptionPane.showMessageDialog(null, "Nomina generada con exito.");
        f.nomina(fichero);
    }
    
    @FXML
    public void totalMes() throws IOException{
        
        String fecha = JOptionPane.showInputDialog("Ingrese el nro del mes");
        FileWriter fichero = null;
        fichero = new FileWriter("Datos/reportes/ingresos/mes-"+fecha+".txt");
        JOptionPane.showMessageDialog(null, "Archivo generado con exito.");
        f.ingresosMensual(fichero,Integer.parseInt(fecha));
    }
    
    @FXML
    public void reporteZ() throws IOException{
        
        JOptionPane.showMessageDialog(null, "AVISO\nEl reporte Z sera generado con el dia actual.");
        FileWriter fichero = null;
        fichero = new FileWriter("Datos/reportes/z/"+fecha_actual+".txt");
        JOptionPane.showMessageDialog(null, "Archivo generado con exito.");
        f.reporteZ(fichero,fecha_actual);
    }
    
}
